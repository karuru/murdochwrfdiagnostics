# MurdochWRFDiagnostics
Get and process some diagnostics fields from WRF output files.

Aim is to write a pure Julia implementation of the [WRF diagnostic tool](https://wrf-python.readthedocs.io)

## Install and usage
```julia
using Pkg
Pkg.add("https://karuru@bitbucket.org/karuru/MurdochWRFDiagnostics")
using MurdochWRFDiagnostics

# daily wrfout variable
f = "wrfout_d01_1970-01-01_00:00:00"

# Get WRF variable
t2 = getvar(f, "T2")

# Get diagnostic
tasmax = getvar(f, "tasmax") # maximum daily temperature as DimensionalArray with metadata
tasmax = getvar(f, "tasmax", false) # normal array i.e. normal Array and no metadata
```

## Diagnostics
| Variable name | Description  | Units  |
| ---- | ---- | ----- |
| cape2d  | 2D CAPE (MCAPE/MCIN/LCL/LFC)  | J kg-1 ; J kg-1 ; m ; m  |
| geopotential  | Geopotential  | m2 s-2 |  
| geopotentialheight | Geopotential height | m |
| lwup  | Upward longwave flux at ground surface | W/m² |
| pracc    | Accumalated precip | mm |
| pressure  | Full Model Pressure | Pa |
| rh    | Relative Humidity | % |
| slp  | Sea Level Pressure | hPa |
| tas    | Mean near surface air temperature | K |
| tasmax  | Maximum near surface air temperature | K |
| tasmin  | Minimum near surface air temperature | K |
| th    | Potential Temperature (θ) | K |
| tk    | Temperature  | K |
| ua    | U-component of Wind on Mass Points | m s-1 |
| uvmet | U and V Components of Wind Rotated to Earth Coordinates | m s-1 ||
| uvmet10 | 10m U and V Components of Wind Rotated to Earth Coordinates | m s-1 |
| uvmet_wspd_wdir | Wind Speed and Direction Rotated to Earth Coordinates | m s-1 
| uvmet10_wspd_wdir | 10m Wind Speed and Direction Rotated to Earth Coordinates | m s-1 |
| va    | V-component of Wind on Mass Points | m s-1 |
| wa    | W-component of Wind on Mass Points | m s-1 |
| wspd  | Wind Speed in Grid Coordinates | m s-1 |
| wdir  | Wind Direction in Grid Coordinates | m s-1 |
| wspd_wdir | Wind Speed and Direction in Grid Coordinates | m s-1 |
| wspd_wdir10 | Wind Speed and Direction in Grid Coordinates | m s-1 |


## NOTES
- A number of diagnostics need to be converveted from fortran to julia 
- Possible refactoring of code
- need to think about how best to rewrite formatdims(A::NcVar) (in array.jl line 181 ) to account for the use case specified by formatdims(A:NcFile) (in array.jl 92).
- possibly make use of unitful so that we can easly convert between units
- uvmet: calculate when diagnostics when rotation is required. ie. projection is 0,3,6 