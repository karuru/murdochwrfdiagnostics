###
# Return relative humidity
###
function rh end
function rh(nc, bool::Bool)
    qvapor = getvar(nc, "QVAPOR", bool)
    map!(x->x < 0 ? 0 : x, qvapor, qvapor)
    t = getvar(nc, "tk", false) # tk ((pressure/1e5))^(RD/CP))) * potentialtemperature
    full_p = getvar(nc, "pressure", false)
    relhum = rh(qvapor, full_p, t)
end
function rh(::Val{true}, nc::NcFile)
    relhum = rh(nc, true)
    relhum.metadata["description"] = "Relative Humidity"
    relhum.metadata["units"] = "%"
    rebuild(relhum, "Relative Humidity")
end

rh(::Val{false}, nc::NcFile) = rh(nc, false)

###
#    rh(qvapor, full_p, tk)
###
function rh(qvapor, full_p, t)
    elementtype = eltype(t)
    @unpack T_BASE, EZERO, ESLCON1, ESLCON2, CELKEL, RD, RV, EPS = Constants{elementtype}()

    const1= convert(elementtype, 1)
    constpoint01 = convert(elementtype, 0.01)
    const100 = convert(elementtype, 100)
    const0 = convert(elementtype, 0)

    relhum = similar(qvapor)
    for idx in eachindex(qvapor)
        es = EZERO * exp(ESLCON1 * (t[idx] - CELKEL)/(t[idx] - ESLCON2))
        qvs = EPS * es/(constpoint01 * full_p[idx] - (const1 - EPS) *  es)
        relhum[idx] = const100 * max(min(qvapor[idx]/qvs, const1), const0)
    end
    relhum
end
