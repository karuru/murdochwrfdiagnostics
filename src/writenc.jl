"""
    writenc(nc::WRFVar, fname, gatts)

write nc to disk
"""
function writenc(nc::WRFVar; fname = "", gatts = Dict())
    nam = String(name(nc))
    varatts = Dict(
        "standard_name" => nam,
        "long_name" => get(metadata(nc), "description", " "),
        "units" => get(metadata(nc), "units", " "),
        "_FillValue" => missingval(nc)
    )

    lonatts = Dict(
        "standard_name" => "longitude",
        "long_name" => "longitude",
        "units" => get(metadata(nc), "lonunits", " "),
        "axis" => "X"
    )

    latatts = Dict(
        "standard_name" => "latitude",
        "long_name" => "latitude",
        "units" => get(metadata(nc), "latunits", " "),
        "axis" => "Y"
    )


    timeunits = "hours since 1900-01-01 00:00:00"
    timevals = val(nc.dims[end])
    timeenc = timeencode.(timevals, timeunits, "standard")
    timeenc = convert.(Int, timeenc)

    timeatts = Dict(
        "standard_name" => "Time",
        "calendar" => "standard",
        "units" => timeunits,
        "axis" => "T"
    )
    startdate = join(string.(yearmonthday(timevals[1]), pad = 2))
    enddate = join(string.(yearmonthday(timevals[end]), pad = 2))
    fname = if length(fname) == 0
        string(nam, "_", startdate, enddate, ".nc")
    else
        string(fname, "_", startdate, enddate, ".nc")
    end
    isfile(fname) && rm(fname)

    ncdata = data(nc)
    nccreate(fname, "time", "time", size(nc)[end], atts = timeatts, gatts = gatts; t=NC_INT);

    # Lat/Lon can be in 1 or 2 dimensions.
    if ndims(metadata(nc)["lat"]) == 2
        nccreate(fname, "lat", "lon", size(nc, 1), "lat", size(nc, 2), atts = latatts; t=NC_FLOAT);
        nccreate(fname, "lon", "lon", size(nc, 1), "lon", size(nc, 2), atts = lonatts; t=NC_FLOAT);
    else
        nccreate(fname, "lat", "lat", length(metadata(nc)["lat"]),  atts = latatts; t=NC_FLOAT);
        nccreate(fname, "lon", "lon", length(metadata(nc)["lon"]), atts = lonatts; t=NC_FLOAT);
    end

    # add landmask
    if !isnothing(metadata(nc)["msk"])
        nccreate(fname, "landmask", "lon", size(nc, 1), "lat", size(nc, 2), atts=Dict("long_name" => "LAND MASK (1 FOR LAND, 0 FOR WATER)", "short_name" => "landmask"); t=NC_INT);
    end

    if size(ncdata)[end] == 1
        ncdata = dropdims(ncdata, dims=length(size(ncdata)))
    end
    datasize = length(size(ncdata))
    if datasize == 2
        nccreate(fname, nam, "lon", size(nc, 1),  "lat", size(nc, 2), atts = varatts; t=NC_FLOAT)
    else
        nccreate(fname, nam, "lon", size(nc, 1), lonatts, "lat", size(nc, 2), latatts, "time", size(nc, 3), timeatts, atts = varatts; t=NC_FLOAT)
    end
    ncwrite(timeenc, fname, "time")
    ncwrite(metadata(nc)["lat"], fname, "lat")
    ncwrite(metadata(nc)["lon"], fname, "lon")
    if !isnothing(metadata(nc)["msk"])
        ncwrite(metadata(nc)["msk"], fname, "landmask")
    end

    ncwrite(ncdata, fname, nam)
end
