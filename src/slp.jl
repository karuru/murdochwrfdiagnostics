
###
#    slp(::Val{true}, nc::NcFile)
#
# Return Sea Level Pressure from file with metadata
###

function slp(::Val{true}, nc::NcFile)
    A = getvar(nc, "slp", false)
    dataunits = "hPa"
    datadesc = "sea level pressure"
    dname = "slp"
    rebuild(nc, A, dataunits, datadesc, dname)
end

function slp(::Val{false}, nc::NcFile)

    full_p = getvar(nc, "pressure", false) #p + pb
    unstuggered_full_ph = getvar(nc, "geopotentialheight", false)  #(ph + phb)/G where G=9.81
    full_ph = destagger(unstuggered_full_ph, 3) # on mass grid

    qvapor = getvar(nc, "QVAPOR", false)
    map!(x->x < 0 ? 0 : x, qvapor, qvapor)

    # only get th since we have full_p
    t = MurdochWRFDiagnostics.tk(Val(false), nc, full_p) # ((pressure/1e5))^(RD/CP))) * potentialtemperature

    #  Find least zeta level that is PCONST Pa above the surface.  We
    #  later use this level to extrapolate a surface pressure and
    #  temperature, which is supposed to reduce the effect of the diurnal
    #  heating cycle in the pressure field.

    @unpack EXPON, USSALR, PCONST, TC = Constants{eltype(tk)}()
    if ndims(full_p) == 3
        levels = view(full_p,:,:,:) .> (view(full_p,:,:,1) .- PCONST)
        levels = sum(levels, dims = 3)
    else
        leveldim = (size(full_p,1),size(full_p,2),size(full_p,4))
        levels = fill(0, leveldim...)
        for i in 1:size(full_p,4)
            tmp_levels =  view(full_p,:,:,:,i) .> (view(full_p,:,:,1,i) .- PCONST)
            levels[:,:,i] = sum(tmp_levels, dims = 3);
        end
    end

    # extrapolate the temperature at the surface and down to sea level
    t_surf, t_sea_level = extrapolate_temperature(full_ph, full_p, t, qvapor, levels)
    #

    ridiculus_mm5_test = true
    point005 = convert(eltype(full_ph), 0.005)
    if ridiculus_mm5_test
        l1 = .!(t_sea_level .< TC)
        l2 = t_surf .<= TC
        l3 = l1 .& l2
        for idx in CartesianIndices(l3)
            if l3[idx]
                t_sea_level[idx] = TC
            else
                t_sea_level[idx] =  TC - point005 * (t_surf[idx] - TC)^2
            end
        end
    end

    computeslp(full_p, full_ph, t_sea_level, t_surf)
end

# Extrapolate the temperature at the surface and down to sea level
#
# TODO
# add error checking

function extrapolate_temperature!(t_surf, t_sea_level, z, p, t, q, levels) where T
    @unpack EXPON, USSALR, PCONST = Constants{eltype(t)}()

    # t_surf = Array{eltype(z)}(undef,size(levels))
    # t_sea_level = similar(t_surf)

    c1 = convert(eltype(z),0.608)
    for idx in CartesianIndices(levels)
        klo = max(levels[idx], 1)
        khi = min(klo + 1, size(p,3) - 1) # size(nz)

        ## add error checking
        if klo == khi
            error("Error trapping levels at i = $(idx[1]), j = $(idx[2])")
        end
        ## Generate indices from levels klo, and khi
        klo = CartesianIndex(idx[1], idx[2], klo)
        khi = CartesianIndex(idx[1], idx[2], khi)
        plo = p[klo]
        phi = p[khi]

        qlo = q[klo]
        qhi = q[khi]

        tlo = t[klo] .* (1 .+ c1 .* qlo)
        thi = t[khi] .* (1 .+ c1 .* qhi)

        zlo = z[klo]
        zhi = z[khi]

        p_at_pconst = p[idx,1] - PCONST
        c2 = log(p_at_pconst/phi) * log(plo/phi)
        t_at_pconst = thi - (thi - tlo) * c2
        z_at_pconst = zhi - (zhi - zlo) * c2

        t_surf[idx] = t_at_pconst * (p[idx,1]/p_at_pconst)^EXPON
        t_sea_level[idx] = t_at_pconst + USSALR * z_at_pconst
    end
    t_surf, t_sea_level
end

function extrapolate_temperature(z, p, t, q, levels) where T
    t_surf = Array{eltype(z)}(undef, size(levels))
    t_sea_level = Array{eltype(z)}(undef, size(levels))
    levelsizes = ndims(levels) == 3 ? size(levels,3) : 1

    for i in 1:levelsizes
        # t_surf[:,:,i], t_sea_level[:,:,i] = extrapolate_temperature(z[:,:,:,i], p[:,:,:,i], t[:,:,:,i], q[:,:,:,i], levels[:,:,i])
        # Views used to access arrays so as to avoid memory allocation.
        extrapolate_temperature!(
            view(t_surf,:,:,i),
            view(t_sea_level,:,:,i),
            view(z,:,:,:,i),
            view(p,:,:,:,i),
            view(t,:,:,:,i),
            view(q,:,:,:,i),
            view(levels,:,:,i)
            )
    end
    #t_surf and t_sea_level are updated in extrapolate_temperature!
    t_surf, t_sea_level
end

# compute slp
function computeslp(p::AbstractArray{T,3}, z, t_sea_level, t_surf) where T
    @unpack G, RD = Constants{eltype(z)}()
    hpa = convert(eltype(G), 0.01)
    @. hpa * (p[:,:,1] * exp((2 * G * z[:,:,1])/(RD * (t_sea_level + t_surf))))
end
function computeslp(p::AbstractArray{T,4}, z, t_sea_level, t_surf) where T
    @unpack G, RD = Constants{eltype(z)}()
    hpa = convert(eltype(G), 0.01)
    @. hpa * (p[:,:,1,:] * exp((2 * G * z[:,:,1,:])/(RD * (t_sea_level + t_surf))))
end
