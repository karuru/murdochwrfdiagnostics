```
Upward longwave radiation at surface
```
function lwup(nc::NcFile)
    # tsk^4 * σ * emiss
    σ =  5.670374419e-8 # stefan-boltzman constant
    A = NetCDF.readvar(nc, "TSK")   
    @. A = A^4 * σ
    A .= A .* NetCDF.readvar(nc, "EMISS")
end 

lwup(::Val{false}, nc::NcFile) = lwup(nc)

function lwup(::Val{true}, nc::NcFile)
    A = lwup(nc)
    dataunits = "W m-2"
    datadesc = "UPWARD LONGWAVE FLUX AT GROUND SURFACE"
    dname = "lwup"
    rebuild(nc, A, dataunits, datadesc, dname)
end