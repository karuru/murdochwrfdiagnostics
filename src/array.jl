abstract type AbstractWRFVar{T,N,D,A} <: AbstractDimArray{T,N,D,A} end

const StandardIndices = Union{AbstractArray,Colon,Integer}

# Level diagnostic dimension. eg. used by cape2d
@dim Diag ZDim "Diagnostic"
@dim Vert ZDim "bottom_top"
@dim VertStag ZDim "bottom_top_stag"
@dim Soil ZDim "soil_layers_stag"
@dim Lat YDim "south_north"
@dim LatStag YDim "south_north_stag"
@dim Lon XDim "west_east"
@dim LonStag XDim "west_east_stag"

###
"""
    WRFVar{T,N}
Represents WRF variable type
"""
struct WRFVar{T, N, D<:Tuple,R<:Tuple, A<:AbstractArray{T,N}, Na<:Symbol, Me, Mi} <: AbstractWRFVar{T,N,D,A}
    data::A
    dims::D
    refdims::R
    name::Na
    metadata::Me
    missingvalue::Mi
end

function WRFVar(nc::NcFile, vname)
    A = nc[vname]

    @unpack DEFAULT_FILL = Constants{eltype(A)}()
    fillvalue = get(A.atts, "_FillValue", DEFAULT_FILL)

    lat, latunits = getlat(nc)
    lon, lonunits = getlon(nc)

    msk = getlandmask(nc)

    d = DD.format(A)

    WRFVar(
        NetCDF.readvar(A),
        d,
        (),
        Symbol(A.name),
        Dict(
            "units" => units(A),
            "description" => description(A),
            "lat" => lat,
            "lon" => lon,
            "msk" => msk,
            "latunits" => latunits,
            "lonunits" => lonunits
            ),
        fillvalue
        )
end

times(v::NcFile) = times(NetCDF.readvar(v["Times"]))
times(A::Array{NetCDF.ASCIIChar,2}) = map(idx->times(A[:,idx]), 1:size(A,2))
times(A::Array{NetCDF.ASCIIChar,1}) = DateTime(replace(join(A), "_" => "T"))

function mergevars(A)
    dimsizes = size(A[1])[1:end - 1]
    timesize = size(A[1])[end]
    B = Array{eltype(DD.data(A[1]))}(undef, dimsizes..., timesize * length(A))
    timearr = Array{eltype(val(dims(A[1]),Ti))}(undef, timesize * length(A))
    colonsize = Base.OneTo.(dimsizes)

    for idx in eachindex(A)
        timestep = ((idx * timesize) + 1) - timesize: (timesize * idx)
B[colonsize..., timestep] = DD.data(A[idx])
        timearr[timestep] = val(dims(A[idx], Ti))
    end
    # get XY or XYZ dimensions.
    xyz = filter(x -> name(x) !== :Time, dims(A[1]))
    ti = Ti(timearr)
    d = DD.format(B, ((xyz)..., ti))

    # rebuild(A[1], B, d)
    WRFVar(B, d, (), name(A[1]), metadata(A[1]), missingval(A[1]))
end

#DimensionalData.rebuildsliced(A::AbstractWRFVar, data, I, name=name(A)) =
    #rebuild(A, data, DimensionalData.slicedims(A, I)..., Symbol(name), metadata(A), missingval(A))
DD.rebuild(A::AbstractWRFVar, data, dims::Tuple, refdims, name, metadata, missingval=missingval(A)) =
    WRFVar(data, DD.format(dims, data), refdims, Symbol(name), metadata, missingval)
DD.rebuild(A::AbstractWRFVar, name) =
    rebuild(A, DD.data(A), dims(A), refdims(A), Symbol(name), metadata(A), missingval(A))

function DD.rebuild(nc::NcFile, A, dataunits, datadesc, dname; kwargs...)
    # need to think about how best to use format(A::NcVar) in this situation where we manually specifiy the dimensions
	kw = Dict(kwargs)
	@unpack DEFAULT_FILL = Constants{eltype(A)}()
	fillvalue = DEFAULT_FILL
	lat, latunits = getlat(nc)
	lon, lonunits = getlon(nc)
	msk = getlandmask(nc)
	ti = Ti(times(nc))
	x = Lon(get(kw, :x, axes(A, 1)))
	y = Lat(get(kw, :y, axes(A, 2)))
	if !isempty(kw)
		diag = Diag(get(kw, :diag, axes(A, ndims(A))))
        ndimsA = ndims(A)
        d = if ndimsA == 4
		    DimensionalData.format(A, (x, y, ti, diag))
        elseif ndimsA == 5
           z = Vert(get(kw, :z, axes(A, 3)))
           DimensionalData.format(A, (x, y, z, ti, diag))
        end
	else
		d = DimensionalData.format((x,y,ti), A)
	end

	WRFVar(
		A,
		d,
		(),
		Symbol(dname),
		Dict(
			"units" => dataunits,
			"description" => datadesc,
			"lat" => lat,
			"lon" => lon,
			"msk" => msk,
			"latunits" => latunits,
			"lonunits" => lonunits
			),
		fillvalue
		)
end
Base.parent(A::AbstractWRFVar) = DD.data(A)

### Interface Methods and utilities
"""
    missingval(x)
Returns the value representing missing data in the dataset
"""
function missingval end
missingval(x) = missing
missingval(A::AbstractWRFVar) = A.missingvalue

function getmeta end
getmeta(D::Dict, meta::String) = get(D, meta, nothing)
getmeta(A::AbstractWRFVar, meta::String) = getmeta(metadata(A), meta)
getmeta(A::NcVar, meta) = getmeta(A.atts, meta)

units(A) = getmeta(A, "units")
description(A) = getmeta(A, "description")

getlat(nc) = getlatlon(nc, "XLAT")
getlon(nc) = getlatlon(nc, "XLONG")
function getlatlon(nc::NcFile, var)
    dimsize = haskey(nc.vars, var) ? ndims(nc[var]) : 0
    latlon = if dimsize == 3
        dropdims(NetCDF.readvar(nc, var, count =[-1,-1,1]), dims = 3)
    elseif dimsize > 0
        NetCDF.readvar(nc, var)
    else
        nothing
    end
    attribute = isnothing(latlon) ? latlon : get(nc[var].atts, "units", nothing)

    latlon, attribute
end

function getlandmask(nc::NcFile)
	var = "LANDMASK"
	dimsize = haskey(nc.vars, var) ? ndims(nc[var]) : 0
    landmask = if dimsize == 3
        dropdims(NetCDF.readvar(nc, var, count =[-1,-1,1]), dims = 3)
	elseif dimsize > 0
        NetCDF.readvar(nc, var)
    else
        nothing
	end
end

# programatically set array dimensions
function DD.format(A::NcVar)
    d = Dict(  
        "bottom_top" => Vert,  
        "bottom_top_stag" =>  VertStag, 
        "soil_layers_stag" => Soil, 
        "south_north" => Lat, 
        "south_north_stag" => LatStag, 
        "west_east" => Lon, 
        "west_east_stag" => LonStag,
        "Time" => Ti
    ) 
    D = map(enumerate(A.dim)) do (k,v) 
        if v.name != "Time"
           d[v.name](axes(A, k))
        else
           Ti(times(A.ncfile))
        end
    end
    DD.format(Tuple(D), A)
end