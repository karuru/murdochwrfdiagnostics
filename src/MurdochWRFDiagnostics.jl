module MurdochWRFDiagnostics
using Reexport, Parameters, ProgressMeter, Statistics, wrfuser_jll
@reexport using DimensionalData
using DimensionalData.Dimensions: ZDim, XDim, YDim, @dim
@reexport using NetCDF
@reexport using Dates
@reexport using CFTime

const DD = DimensionalData

include("array.jl")
export WRFVar, mergevars, units, description, 
       Lat, LatStag, Lon, LonStag, Soil, Vert, 
       VertStag, Diag
#include("../userdiagnostics/userdiagnostics.jl")
include("constants.jl")
include("selector.jl")
export daily, monthly, climatological, yearly
include("pressure.jl")
include("slp.jl")
include("geopotential.jl")
include("destagger.jl")
include("temperature.jl")
include("precipitation.jl")
include("relativehumidity.jl")
include("cape.jl")
include("wind.jl")
include("uvmet.jl")
include("upwardlongwave.jl")
include("writenc.jl")
export writenc
include("getvar.jl")
export getvar

include("interpolations.jl")
export interplev


end # module
