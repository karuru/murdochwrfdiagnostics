function cape2d(::Val{true}, nc::NcFile)
	A = cape2d(Val(false), nc)
	dataunits = "J kg-1 ; J kg-1 ; m ; m"
    datadesc = "mcape ; mcin ; lcl ; lfc"
    dname = "cape2d"
	rebuild(nc, A, dataunits, datadesc, dname; diag = [:mcape, :mcin, :lcl, :lfc])
end

function cape2d(::Val{false}, nc::NcFile)
	# multiply by 1
	ter = getvar(Float64, nc, "HGT")
	psfc = getvar(Float64, nc, "PSFC")
	qvapor = getvar(Float64, nc, "QVAPOR")

	full_p = getvar(Float64, nc, "pressure") #p + pb;
	full_ph = getvar(nc, "geopotential", false) #(ph + phb)/G where G=9.81;
	full_ph = MurdochWRFDiagnostics.destagger(full_ph, 3) ./ 9.81# on mass grid;

	t = MurdochWRFDiagnostics.tk(Val(false), nc, full_p)
	# t = convert(Array{Float64}, t)[:,:,:,1] # ((pressure/1e5))^(RD/CP))) * potentialtemperature;
	map!(x->x < 0 ? 0 : x, qvapor, qvapor)
	reverse!(full_p, dims = 3)
	reverse!(t, dims = 3)
	reverse!(qvapor, dims = 3)
	reverse!(full_ph, dims = 3)


	@unpack ERRLEN, PA_TO_HPA, DEFAULT_FILL_FLOAT = MurdochWRFDiagnostics.Constants{eltype(full_p)}()
	# pressure converted from Pa to hPa
	# psfc to hPa
	map!(x -> x * PA_TO_HPA, full_p, full_p);
	map!(x -> x * PA_TO_HPA, psfc, psfc);

	dimensionsizes = size(full_p)
	if ndims(full_p) == 4
		mix, mjy, mkzh, mlt = dimensionsizes
		results = Array{eltype(full_p)}(undef, mix, mjy, mlt, 4)
	else
		mix, mjy, mkzh = dimensionsizes
		results = Array{eltype(full_p)}(undef, mix, mjy, 4)
	end

	cape = Array{Float64}(undef, mix, mjy, mkzh)
	cin = similar(cape)

	ght_new = Array{Float64}(undef, mkzh, mix, mjy)
	prsf = similar(ght_new)
	prs_new = similar(ght_new)
	tmk_new = similar(ght_new)
	qvp_new = similar(ght_new)

	i3dflag = 0
	ter_follow = 1
	errmsg = " "
	errstat = 0
	cmsg=  NaN
	fname = joinpath(@__DIR__, "psadilookup.dat")

	cape2d!(results, dimensionsizes, full_p, t, qvapor, full_ph, ter, psfc,
			cape, cin, prsf, prs_new, tmk_new, qvp_new, ght_new,
			cmsg, ter_follow,
			fname, errstat, errmsg)

	return results
end

function cape2d!(results, dimensionsizes::NTuple{3}, full_p, t, qvapor, full_ph, ter, psfc,
		cape, cin, prsf, prs_new, tmk_new, qvp_new, ght_new,
		cmsg, ter_follow,
		fname, errstat, errmsg)
	mix, mjy, mkzh = dimensionsizes
	ccall((:dcapecalc2d_,
			wrfuser_jll.WRFUser),
			Cvoid,
			(Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64},
			 Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64},
			 Ref{Float64}, Ref{Int}, Ref{Int}, Ref{Int}, Ref{Int},
			 Ptr{UInt8}, Ref{Int}, Ptr{UInt8}, UInt32, UInt32),
			full_p, t, qvapor, full_ph, ter, psfc,
			cape, cin, prsf, prs_new, tmk_new, qvp_new, ght_new,
			cmsg, mix, mjy, mkzh, ter_follow,
			fname, errstat, errmsg, sizeof(fname), sizeof(errmsg)
	)

	results[:,:,1] .= cape[:,:,end]
	results[:,:,2] .= cin[:,:,end] #cape
	results[:,:,3] .= cin[:,:,end-1] #lcl
	results[:,:,4] .= cin[:,:,end-2] #lfc
end

function cape2d!(results, dimensionsizes::NTuple{4}, full_p, t, qvapor, full_ph, ter, psfc,
		cape, cin, prsf, prs_new, tmk_new, qvp_new, ght_new,
		cmsg, ter_follow,
		fname, errstat, errmsg)
	mix, mjy, mkz, mlt = dimensionsizes

	for idx in 1:mlt
		V = view(results,:,:,idx,:)
		Vfull_p = view(full_p,:,:,:,idx)
		Vt  = view(t,:,:,:,idx)
		Vqvapor = view(qvapor,:,:,:,idx)
		Vfull_ph = view(full_ph,:,:,:,idx)
		Vter =  view(ter,:,:,idx)
		Vpsfc = view(psfc,:,:,idx)
		cape2d!(V, (mix, mjy, mkz), Vfull_p, Vt, Vqvapor, Vfull_ph, Vter, Vpsfc,
				cape, cin, prsf, prs_new, tmk_new, qvp_new, ght_new,
				cmsg, ter_follow,
				fname, errstat, errmsg)
	end
end
