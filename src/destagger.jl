"""
    destagger(var::AbstractArray, stagger_dimension::Integer)

Return the variable on the unstaggered grid.

Destaggers the variable, at a given dimension, by taking the average of the
values located on either side of the grid box.
"""
function destagger(variable::AbstractArray, stagger_dimension::Integer)
    shape = size(variable)

    # Dynamically building array with ranges
    # create array that contains shape, so that we can make changes. Tuples are immutable.
    # ... = splat
    dimrange1 = [1:stp for stp in shape]
    dimrange1[stagger_dimension] = 1:dimrange1[stagger_dimension].stop - 1
    dimrange2 = [1:stp for stp in shape]
    dimrange2[stagger_dimension] = 2:dimrange2[stagger_dimension].stop

    A = (view(variable,dimrange1...) .+ view(variable,dimrange2...)) .* convert(eltype(variable),0.5)
    if typeof(A) <: WRFVar
        A = rebuild(A, name(variable))
    end
    A
end
