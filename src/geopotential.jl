# """
# Total geopotential: (PH + PHB)
# """
function geopotential end
function geopotential(nc, bool::Bool)
    full_ph = getvar(nc, "PH", bool)
    full_ph .= full_ph .+ getvar(nc, "PHB", false)
end
geopotential(::Val{false}, nc::NcFile) = geopotential(nc, false)

function geopotential(::Val{true}, nc::NcFile)
    full_ph = geopotential(nc, true)
    full_ph.metadata["description"] = "Geopotential (ph + phb)"
    rebuild(full_ph, "geopotential")
end

# """
# Total geopotential height: (PH + PHB)/9.81
# """
function geopotentialheight end
function geopotentialheight(nc, bool::Bool)
    geopt =  getvar(nc, "geopotential", bool)
    @unpack G = Constants{eltype(geopt)}()
    geopt .= geopt ./ G
end

geopotentialheight(::Val{false}, nc::NcFile) = geopotentialheight(nc, false)

function geopotentialheight(::Val{true}, nc::NcFile)
    geopt =  geopotentialheight(nc, true)
    geopt.metadata["description"] = "Geopotential height (geopotential/9.81)"
    geopt.metadata["units"] = "m"
    rebuild(geopt, "Geopotential Height")
end
