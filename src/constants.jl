@with_kw struct Constants{T}
	ERRLEN::Integer = 512
	ALGERR::Integer = 64

	WRF_EARTH_RADIUS::T = 6.37e6
	T_BASE::Integer = 300
	RAD_PER_DEG::T = π/180
	DEG_PER_RAD::T = 180/π
	DEFAULT_FILL::T = 9.9692099683868690e36
	DEFAULT_FILL_INT8::Int8 = -127
	DEFAULT_FILL_INT16::Int16 = -32767
	DEFAULT_FILL_INT32::Int32 = -2147483647
	DEFAULT_FILL_FLOAT::Float64 = DEFAULT_FILL
	DEFAULT_FILL_DOUBLE = DEFAULT_FILL
	DEFAULT_FILL_CHAR = Char(0)

	P1000MB::T = 1e5
	PCONST::T = 1e4
	TC::T = 273.16 + 17.5
# j/k/kg
	RD::T = 287
	RV::T = 461.60
#RV::T = 461.50
#  j/k/kg  note: not using bolton's value of 1005.7
	CP::T = 1004.50 # 7 * RD/2
#CP::T = 10040

	G::T = 9.810
	USSALR::T = 0.0065  # deg C per m

	CELKEL::T = 273.150
	CELKEL_TRIPLE::T = 273.160

# hpa
	EZERO::T = 6.1120
	ESLCON1::T = 17.670
	ESLCON2::T = 29.650
	EPS::T = 0.6220
	GAMMA::T = RD/CP
#  cp_moist=cp*(1.+cpmd*qvp)
	CPMD::T = .8870
#  rgas_moist=rgas*(1.+rgasmd*qvp)
	RGASMD::T = .6080
#  gamma_moist=gamma*(1.+gammamd*qvp)
	GAMMAMD::T = RGASMD - CPMD
	TLCLC1::T = 2840
	TLCLC2::T = 3.50
	TLCLC3::T = 4.8050
	TLCLC4::Integer = 55
#  k
	THTECON1::Integer = 3376
	THTECON2::T = 2.540
	THTECON3::T = .810

	ABSCOEFI::T = .2720  # cloud ice absorption coefficient in m^2/g
	ABSCOEF::T = .1450   # cloud water absorption coefficient in m^2/g

	GAMMA_SEVEN::T = 720
	RHOWAT::T = 1000
	RHO_R::T = RHOWAT
	RHO_S::T = 100
	RHO_G::T = 400
	ALPHA::T = 0.2240

	SCLHT::T = RD*256/G
	EXPON::T =  RD*USSALR/G
	EXPONI::T =  1/EXPON

	PA_TO_HPA = .01
	PA_TO_TORR = 760.0/101325.0
	PA_TO_MMHG = PA_TO_TORR * 1.000000142466321
	PA_TO_ATM = 1.0 / 1.01325E5
	MPS_TO_KTS = 1.94384
	MPS_TO_KMPH = 3.60
	MPS_TO_MPH = 2.23694
	MPS_TO_FPS = 3.28084
	M_TO_KM = 1.0/1000.0
	M_TO_DM = 1.0/10.0
	M_TO_FT = 3.28084
	M_TO_MILES = .000621371
end
