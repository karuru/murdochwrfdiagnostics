
function _wind(nc::NcFile, var, stagdim, bool=true)  
    # replace eg. U10 or U with UU so that if 
    # U10 or U are missing we then call UU
    altvar = replace(var, r"([UV])\d*" => s"\g<1>\g<1>") 
    newvar = getkey(nc.vars, var, altvar) 
    A = getvar(nc, newvar, bool)
    if !((newvar == var) && any(x -> x == var, ("U10", "V10")))
        A = destagger(A, stagdim)
    end

    if any(x -> x == var, ("U10", "V10")) && newvar == altvar
        # make sure that we are returning only the lowest level (10m)
        # if newvar == UU and altvar == UU and var is either U10 or V1 0
        # ie. if we call UU or VV make sure that we return wind at 10m
        A = A[:,:,1,:]
    end
    A
end

ua(::Val{false}, nc::NcFile) = _wind(nc, "U", 1, false)
function ua(::Val{true}, nc::NcFile)
    A = _wind(nc, "U", 1, true)
    A.metadata["description"] = "destaggered u-wind component"
    rebuild(A, "ua")
end


va(::Val{false}, nc::NcFile) = _wind(nc, "V", 2, false)
function va(::Val{true}, nc::NcFile)
    A = _wind(nc, "V", 2, true)
    A.metadata["description"] = "destaggered v-wind component"
    rebuild(A, "va")
end

wa(::Val{false}, nc::NcFile) = _wind(nc, "W", 3, false)
function wa(::Val{true}, nc::NcFile)
    A = _wind(nc, "W", 3, true)
    A.metadata["description"] = "destaggered w-wind component"
    rebuild(A, "wa")
end

# Wind speed
function _wspd(nc, wind::Tuple, meta = true)
    u = _wind(nc, wind[1], 1, false)
    u .= u.^2
    v = _wind(nc, wind[2], 2, meta)
    v .= v.^2
    sqrt.(v .+ u)
end

wspd(::Val{false}, nc::NcFile) = _wspd(nc, ("U", "V"), false)
wspd10(::Val{false}, nc::NcFile) = _wspd(nc, ("U10", "V10"), false)

function wspd(::Val{true}, nc::NcFile)
    A = _wspd(nc, ("U", "V"), true)
    A.metadata["description"] = "wspd in projection space"
    rebuild(A, "wspd")
end

function wspd10(::Val{true}, nc::NcFile)
    A = _wspd(nc, ("U10", "V10"), true)
    A.metadata["description"] = "10m wspd in projection space"
    rebuild(A, "wspd")
end

# Wind direction
function _wdir(nc, wind::Tuple, meta = true)
    u = _wind(nc, wind[1], 1, meta)
    v = _wind(nc, wind[2], 2, false)
    @unpack DEG_PER_RAD = Constants{Float64}()
    winddir = similar(u)
    winddir .= atan.(v,u)
    map!(x -> mod((270 - x * DEG_PER_RAD), 360), winddir, winddir)
end

wdir(::Val{false}, nc::NcFile) = _wdir(nc, ("U", "V"),  false)
wdir10(::Val{false}, nc::NcFile) = _wdir(nc, ("U10", "V10"),  false)

function wdir(::Val{true}, nc::NcFile)
    A = _wdir(nc, ("U", "V"), true)
    A.metadata["description"] = "wdir in projection space"
    rebuild(A, "wdir")
end

function wdir10(::Val{true}, nc::NcFile)
    A = _wdir(nc, ("U10", "V10"), true)
    A.metadata["description"] = "10m wdir in projection space"
    rebuild(A, "wdir")
end

# Wind speed and direction at all levels
function _wspd_wdir(nc::NcFile, wind::Tuple)
    speed = _wspd(nc, wind, false)
    direction = _wdir(nc, wind, true)
    dimaxes = axes(speed)
    A = Array{eltype(speed)}(undef, length.(dimaxes)..., 2)
    A[dimaxes...,1] = speed
    A[dimaxes...,2] = direction
    A
end

wspd_wdir(::Val{false}, nc::NcFile) = _wspd_wdir(nc, ("U", "V"))
function wspd_wdir(::Val{true}, nc::NcFile)
    A = _wspd_wdir(nc, ("U", "V"))
    dataunits = "m s⁻¹"
    datadesc = "wspd, wdir in projection space"
    dname = "wspd_wdir"  
    rebuild(nc, A, dataunits, datadesc, dname; diag = [:wspd, :wdir])
end

# 10m wind speed and direction
wspd_wdir10(::Val{false}, nc::NcFile) =  _wspd_wdir(nc, ("U10", "V10"))
function wspd_wdir10(::Val{true}, nc::NcFile) 
   A =  _wspd_wdir(nc, ("U10", "V10"))
   dataunits = "m s⁻¹"
   datadesc = "10m wspd, wdir in projection space"
   dname = "wspd_wdir10"  
   rebuild(nc, A, dataunits, datadesc, dname; diag = [:wspd, :wdir])
end