
const diagnostic = Dict(
    "pressure" => pressure,
    "slp" => slp,
    "tas" => tas,
    "tasmax" => tasmax,
    "tasmin" => tasmin,
    "tk" => tk,
    "th" => potentialtemperature,
    "pracc" => pracc,
    "geopotential" => geopotential,
    "geopotentialheight" => geopotentialheight,
    "rh" => rh,
    "cape2d" => cape2d,
    "wspd_wdir" => wspd_wdir,
    "wspd_wdir10" => wspd_wdir10,
    "wa" => wa,
    "ua" => ua,
    "va" => va,
    "wdir" => wdir,
    "wspd" => wspd,
    "uvmet" => uvmet,
    "uvmet10" => uvmet10,
    "uvmet_wspd" => uvmet_wspd,
    "uvmet_wdir" => uvmet_wdir,
    "uvmet10_wspd" => uvmet10_wspd,
    "uvmet10_wdir" => uvmet10_wdir,
    "uvmet_wspd_wdir" => uvmet_wspd_wdir,
    "uvmet10_wspd_wdir" => uvmet10_wspd_wdir,
    "lwup" => lwup
)
"""
    getvar(f::String, variable::String, meta::Bool=true)

Get diagnostic/variable. return WRFVar if meta=true else retrun Array if meta=false. Default behaviour is to return a WRFVar

Example
getvar("wrfout_d01_1980-01-01_00:00:00", "T2") or
getvar("wrfout_d01_1980-01-01_00:00:00", "T2", false)
getvar("wrfout_d01_1980-01-01_00:00:00", "pracc")
getvar("wrfout_d01_1980-01-01_00:00:00", "pracc", false)
"""
function getvar(f::String, vname::String, meta::Bool=true)
    NetCDF.open(f) do x getvar(x, vname, meta) end
end
# return variable without metadata. ie. just Array
# return diagnostic without  metadata. ie just Array
getvar(::Val{false}, ::Val{true}, nc::NcFile, vname) = getvar(Val(false), nc, vname)
getvar(::Val{false}, ::Val{false}, nc::NcFile, vname) = diagnostic[vname](Val(false),nc)

# return variables with metadata ie WRFVar
getvar(::Val{true}, ::Val{false}, nc::NcFile, vname) = diagnostic[vname](Val(true),nc)

getvar(::Val{true}, ::Val{true}, nc::NcFile, vname) = WRFVar(nc, vname)
getvar(::Val{false}, nc::NcFile, vname) = NetCDF.readvar(nc, vname)

getvar(nc::NcFile, vname::String, meta::Bool=true) = getvar(Val(meta), Val(haskey(nc.vars, vname)), nc, vname)


# Arrays passsed to fortran should be float 64
getvar(::Type{<:Float64}, nc::NcFile, vname::String) = convert(Array{Float64}, getvar(nc, vname, false))
