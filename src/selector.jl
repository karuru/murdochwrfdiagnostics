times(A, f::Function) = unique(f.(val(A, Ti))) 

function selection end
"""
    selection(ymd::Function, f::Function, A::WRFVar)
apply function to selection
eg. used to get daily precipitation totals from hourly values

selection(yearmonthday, sum, A)

or

get monthly totals from daily values
selection(yearmonth, sum, A)
"""
function selection(ymd::Function, f::Function, A::WRFVar)
    x = dims(A, Lon)
    y = dims(A, Lat)
    t1 = times(A, ymd)
    t2 = Array{TimeType,1}(undef, length(t1))
    ## Need to ensure that time dimension mode = Sampled(span=Irregular(), sampling=Points(), order=Ordered())
    ## In order for selection to keep on working as expected. For now force
    ## dimension mode
    t2 = Ti(t2, mode = mode(A, Ti))
    A1 = Array{eltype(A)}(undef, length(x), length(y), length(t1))

    for ti in 1:length(t1)
        # apply function .eg. sum
        s = f(view(A, Ti(Where(x -> ymd(x) == t1[ti]))), dims = Ti)
        ### loop to deal with missing values.
        A1[:,:,ti] = data(s)
        t2.val[ti] = val(s, Ti)[1]
    end
    WRFVar(A1, DimensionalData.formatdims(A1,(x,y,t2)), (), name(A), metadata(A), MurdochWRFDiagnostics.missingval(A))
end

"""
    daily(f::Function, A::WRFVar)
Returns daily values
"""
function daily end
daily(f::Function, A::WRFVar) = selection(yearmonthday, f, A)

function monthly end
monthly(f::Function, A::WRFVar) = selection(yearmonth, f, A)

function yearly end
yearly(f::Function, A::WRFVar) = selection(year, f, A)

function climatological end
climatological(f::Function, A::WRFVar) = selection(month, f, A)
