function _norotate(wind::Tuple)
    dimaxes = axes(wind[1])
    A = Array{eltype(wind[1])}(undef, length.(dimaxes)..., 2)
    A[dimaxes...,1] = wind[1]
    A[dimaxes...,2] = wind[2]
    A
end
function _norotate(nc, wind::Tuple, map_proj)
    true_lat1 = nc.gatts["TRUELAT1"]  
    true_lat2 = nc.gatts["TRUELAT2"]
    radians_per_degree = π/180

    cen_lon = try
        nc.gatts["STAND_LON"]
    catch
        try
            nc.gatts["CEN_LON"]
        catch 
            error("longitude attributes not found in NetCDF")
        end
    end
    
    varname = getkey(nc.vars, "XLAT_M", "XLAT")
    lat = NetCDF.readvar(nc, vname)

    varname = getkey(nc.vars, "XLONG_M", "XLONG")
    lon = NetCDF.readvar(nc, vname)

    if map_proj == 1
        if (abs(true_lat1 - true_lat2) > 0.1) && (abs(true_lat2 - 90.) > 0.1)
            cone = (log(cos(true_lat1 * radians_per_degree)) -
                    log(cos(true_lat2 * radians_per_degree)))
            cone = (cone /
                    (log(tan((45 -fabs(true_lat1/2.)) * radians_per_degree))
                    - log(tan((45 -fabs(true_lat2/2.)) *
                            radians_per_degree))))
        else
            cone = sin(abs(true_lat1) * radians_per_degree)
        end
    else
        cone = 1
    end
    cen_lon, cone
end
function _uvmet(nc::NcFile, wind::Tuple) 
    u = _wind(nc::NcFile, wind[1], 1, false)
    v = _wind(nc::NcFile, wind[2], 2, false)
    map_proj = nc.gatts["MAP_PROJ"]
    # 1 - Lambert
    # 2 - Polar Stereographic
    # 3 - Mercator
    # 6 - Lat/Lon
    # Note:  NCL has no code to handle other projections (0,4,5) as they
    # don't appear to be supported any longer
    norotate = any(==(map_proj), (0, 3, 6))
    if norotate
        A = _norotate((u,v))
    else
        cen_lon, cone = _norotate(nc, (u,v), map_proj)
    end
    
end

###--------------------------
### NEED TO TEST WHEN ROTATION IS NOT (0,3,6)
### ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
### ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
function _uvmet(u, v, lat, lon, cen_lon, cone)
    nothing
end


uvmet(::Val{false}, nc::NcFile) = _uvmet(nc::NcFile, ("U", "V"))

function uvmet(::Val{true}, nc::NcFile) 
    A = _uvmet(nc::NcFile, ("U", "V")) 
    dataunits = "m s⁻¹"
    datadesc = "earth rotated u,v"
    dname = "uvmet"  
    rebuild(nc, A, dataunits, datadesc, dname; diag = [:u, :v])
end

uvmet10(::Val{false}, nc::NcFile) = _uvmet(nc::NcFile, ("U10", "V10")) 

function uvmet10(::Val{true}, nc::NcFile) 
    A = _uvmet(nc::NcFile, ("U10", "V10")) 
    dataunits = "m s⁻¹"
    datadesc = "10m earth rotated u,v"
    dname = "uvmet10"  
    rebuild(nc, A, dataunits, datadesc, dname; diag = [:u, :v])
end

uvmet_wspd(::Val{false}, nc) = _wspd(nc, ("U", "V"), false)

function uvmet_wspd(::Val{true}, nc) 
    A = _wspd(nc, ("U", "V"), true)
    A.metadata["description"] = "earth rotated wspd"
    rebuild(A, "uvmet_wspd")
end

uvmet10_wspd(::Val{false}, nc) = _wspd(nc, ("U10", "V10"), false)

function uvmet10_wspd(::Val{true}, nc) 
    A = _wspd(nc, ("U10", "V10"), true)
    A.metadata["description"] = "10m earth rotated wspd"
    rebuild(A, "uvmet10_wspd")
end

uvmet_wdir(::Val{false}, nc) = _wdir(nc, ("U", "V"), false)

function uvmet_wdir(::Val{true}, nc) 
    A = _wdir(nc, ("U", "V"), true)
    A.metadata["description"] = "earth rotated wdir"
    rebuild(A, "uvmet_wdir")
end

uvmet10_wdir(::Val{false}, nc) = _wdir(nc, ("U10", "V10"), false)

function uvmet10_wdir(::Val{true}, nc) 
    A = _wdir(nc, ("U10", "V10"), true)
    A.metadata["description"] = "10m earth rotated wdir"
    rebuild(A, "uvmet10_wdir")
end

uvmet_wspd_wdir(::Val{false}, nc::NcFile) =  _wspd_wdir(nc, ("U", "V"))

function uvmet_wspd_wdir(::Val{true}, nc::NcFile) 
   A =  uvmet_wspd_wdir(Val(false), nc)
   dataunits = "m s-1"
   datadesc = "earth rotated wspd, wdir"
   dname = "uvmet_wspd_wdir"  
   rebuild(nc, A, dataunits, datadesc, dname; diag = [:wspd, :wdir])
end

uvmet10_wspd_wdir(::Val{false}, nc::NcFile) =  _wspd_wdir(nc, ("U10", "V10"))

function uvmet10_wspd_wdir(::Val{true}, nc::NcFile) 
   A =  uvmet10_wspd_wdir(Val(false), nc)
   dataunits = "m s-1"
   datadesc = "10m earth rotated wspd, wdir"
   dname = "uvmet10_wspd_wdir" 
   rebuild(nc, A, dataunits, datadesc, dname; diag = [:wspd, :wdir])
end