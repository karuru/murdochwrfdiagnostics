"""
    t2(nc::NcFile)
Get 2m temperatures
"""
t2(::Val{false}, nc) = getvar(nc, "T2", false)
t2(::Val{true}, nc) = WRFVar(nc, "T2")

"""
    tas(nc::NcFile)

Near surface mean air temperature
"""
function tas end
tas(::Val{false}, nc) = tas(t2(Val(false), nc))
tas(nc::Array) = mean(nc, dims = 3)

function tas(::Val{true}, nc)
    d = t2(Val(true), nc)
    d = maximum(d, dims = ndims(d))
    d.metadata["description"] = "Mean near surface air temperature"
    rebuild(d, data(d), dims(d), refdims(d), "tas", metadata(d), missingval(d))
end

"""
    tasmax(nc::NcFile)
Near surface maximum air temperature
"""
function tasmax end
tasmax(::Val{false}, nc) = tasmax(t2(Val(false), nc))
tasmax(nc::Array) = maximum(nc, dims = 3)

function tasmax(::Val{true}, nc)
    d = t2(Val(true), nc)
    d = maximum(d, dims = ndims(d))
    d.metadata["description"] = "Maximum near surface air temperature"
    rebuild(d, data(d), dims(d), refdims(d), "tasmax", metadata(d), missingval(d))
end

"""
    tasmin(nc::NcFile)
Near surface minimum air temperature
"""
function tasmin end
tasmin(::Val{false}, nc) = tasmin(t2(Val(false), nc))
tasmin(nc::Array) = minimum(nc, dims = 3)

function tasmin(::Val{true}, nc)
    d = t2(Val(true), nc)
    d = minimum(d, dims = ndims(d))
    d.metadata["description"] = "Minimum near surface air temperature"
    rebuild(d, data(d), dims(d), refdims(d), "tasmin", metadata(d), missingval(d))
end

"""
    tk(f::NcFile, full_p::AbstactArray)

From potential temperature to temperature (K)
((pressure/1e5))^(RD/CP))) * potentialtemperature

"""
function tk end
function tk(::Val{false}, nc::NcFile)
    t = getvar(nc, "th",false) #potentialtemperature: θ = T + 300
    full_p = getvar(nc, "pressure", false)
    tk!(t, full_p)
end

function tk(::Val{true}, nc::NcFile)
    t = getvar(nc, "th") #potentialtemperature = θ + T_Base
    full_p = getvar(nc, "pressure", false)
    tk!(t, full_p)
    t.metadata["description"] = "temperature"
    t.metadata["units"] = "K"
    rebuild(t, "temp")
end

# only get potential temperature since we have full_p
function tk(::Val{false}, nc::NcFile, full_p::AbstractArray)
    t = getvar(nc, "th", false); #potentialtemperature: T + 300 = θ
    t = convert(Array{eltype(full_p)}, t)
    tk!(t, full_p)
end

# calculate temperature using potential temperature and pressure
# ((pressure/1e5))^(RD/CP))) * potentialtemperature
# potentialtemperature = θ
# pressure = full_p
function tk!(θ, full_p)
    @unpack P1000MB, GAMMA = Constants{eltype(θ)}()
    @. θ = ((full_p/P1000MB)^GAMMA) * θ
end

###
#
# From perturbation potential temperature to total potential temperature
# T + 300 = θ
# """
function potentialtemperature end
function potentialtemperature(nc, bool::Bool)
    θ = getvar(nc,"T", bool)
    @unpack T_BASE = Constants{eltype(θ)}()
    θ .= θ .+ T_BASE
end

potentialtemperature(::Val{false}, nc::NcFile) = potentialtemperature(nc, false)

function potentialtemperature(::Val{true}, nc::NcFile)
    θ = potentialtemperature(nc, true)
    θ.metadata["description"] = "Potential temperature (θ): T + 300"
    θ.metadata["units"] = "K"
    rebuild(θ, "θ")
end
