"""
    pressure(f::NcFile)

Take a file (f) and return total pressure: (P + PB)
"""
function pressure end
function pressure(::Val{false}, nc)
    pres = getvar(nc, "P", false)
    pres .= pres .+ getvar(nc, "PB", false)
end

function pressure(::Val{true}, nc)
    pres = getvar(nc, "P", true)
    pres .= pres .+ getvar(nc, "PB", false)
    pres.metadata["description"] = "total pressure (p + pb)"
    pres.metadata["units"] = "Pa"
    rebuild(pres, "pres")
end

function pressure_to_hpa!(full_p)
    @unpack PA_TO_HPA = Constants{eltype(full_p)}()
    # pressure converted from Pa to hPa
    map!(x -> x * PA_TO_HPA, full_p, full_p)
end
