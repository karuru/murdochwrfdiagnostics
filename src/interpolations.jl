"""
    interplev(data3d, zdata, levels)

Interpolating a 3D var (data3d) to a specific horizontal Level (levels) using pressure level data (zdata).

# Examples
```
fn = "wrfout_d01_1970-01-01_00:00:00"
z = getvar(fn, "geopotential")

# to mass grid point along the levels
z = destagger(z , 3)

p = getvar(fn, "pressure")
p .*= 0.01 # to hPa

ht_500mb = interplev(z, p, 500.0)
```
"""
function interplev(data3d, zdata, levels)
    elemtype = eltype(1.0)
    data3d = convert.(elemtype, data3d)
    zdata = convert.(elemtype, zdata)
    @unpack DEFAULT_FILL = Constants{elemtype}()

    if ndims(levels) > 0
        nlev = ndims(levels)
    else
        levels = [levels]
        nlev = 1
    end

    if ndims(data3d) == 3
        nx, ny, nz = size(data3d)
        dims = (nx, ny, nlev)
    else
        nx, ny, nz, ti = size(data3d)
        dims =  (nx, ny, nlev, ti)
    end
    out2d = Array{elemtype}(undef, dims...)

    interplevel!(data3d.data, out2d, zdata.data, levels, nx, ny, nz, nlev, DEFAULT_FILL)
    out2d
end

function interplevel!(data3d, out2d, zdata, levels, nx, ny, nz, nlev, missingval)
    ccall(
        (:dinterp3dz_, wrfuser_jll.WRFUser),
        Cvoid,
        (Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Float64},
        Ref{Int}, Ref{Int}, Ref{Int}, Ref{Int}, Ref{Float64}),
        data3d, out2d, zdata, levels, nx, ny, nz, nlev, missingval
    )
end

###
# Convert from fortran to Julia
###
# function interplev(data3d, zdata, levels)
#     zdata = zdata .* 1.0
#     data3d = data3d .* 1.0
#     elemtype = eltype(zdata)
#     @unpack DEFAULT_FILL = Constants{elemtype}()
#     if ndims(data3d) == 3
#         nx, ny, nz = size(data3d)
#     else
#         nx, ny, nz, ti = size(data3d)
#     end
#
#     if ndims(levels) > 0
#         nlev = ndims(levels)
#     else
#         levels = [levels]
#         nlev = 1
#
#     end
#     out2d = fill(DEFAULT_FILL, nx, ny, nlev)
#
#     # does vertical coordinate increase or decrease with increasing k?
#     # set offset appropriately
#     ip = 0
#     im = 1
#     if zdata[1,1,1] > zdata[1,1,nz]
#         ip = 1
#         im = 0
#     end
#
#     for lev in 1:nlev
#         for  i in 1:nx
#             for j in 1:ny
#                 dointerp = false
#                 kp = nz
#                 desiredloc = levels[lev]
#                 while (! dointerp) && (kp >= 2)
#                     if (zdata[i,j,kp-im] < desiredloc) && (zdata[i,j,kp-ip] > desiredloc)
#                         w2 = (desiredloc - zdata[i,j,kp-im])/(zdata[i,j,kp-ip] - zdata[i,j,kp-im])
#                         w1 = 1.0 - w2
#                         out2d[i,j, lev] = w1 * data3d[i,j,kp-im] + w2 * data3d[i,j,kp-ip]
#                         dointerp = true
#                     end
#                     kp = kp - 1
#                 end
#             end
#         end
#     end
#     out2d
# end
