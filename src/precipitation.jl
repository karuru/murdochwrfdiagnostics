"""
    pracc(f::String)

Accumulated total  precipitation (rainc + rainnc)
"""
function pracc end
function pracc(nc, bool::Bool)
    # consider bucket in pracc calculation
    # p1 = I_RAINC * bucket + rainc
    # p2 = I_RAINNC * bucket + rainnc
    # pracc = p1 + p2
    # or pracc = rainc + rainnc + ((I_RAINC + I_RAINNC) * bucket)
    bucket = get(nc.gatts, "BUCKET_MM", nothing)
    if !isnothing(bucket)
        rain = getvar(nc, "RAINC", bool)
        rain .+= getvar(nc, "RAINNC", false)
        i_rain = getvar(nc, "I_RAINC", false)
        i_rain .+= getvar(nc, "I_RAINNC", false)
        map!(x -> x * bucket, i_rain, i_rain)
        rain .+= convert(Array{eltype(rain)}, i_rain)
    else
        rain = getvar(nc, "RAINC", bool)
        rain .+= getvar(nc, "RAINNC", false)
    end
    rain
end
pracc(::Val{false}, nc) = pracc(nc, false)

function pracc(::Val{true}, nc::NcFile)
    rain = pracc(nc, true)
    rain.metadata["description"] = "Accumulated Precipitation: RAINC + RAINNC"
    rebuild(rain, "pracc")
end
